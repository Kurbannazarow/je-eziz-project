import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.css'
import Notes from './pages/Notes'
import Create from './pages/Create'

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Notes />} />
        <Route path="create" element={<Create />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
